package com.example.hira.buyit;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class DetailsActivity extends AppCompatActivity {


    ImageView ProductImage;
    TextView Name,Price,Feature;
    Button btnCall,btnSms,btnMail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ProductImage =(ImageView)findViewById(R.id.im_activity_details_image);
        Name = (TextView)findViewById(R.id.tv_details_activity_name);
        Price = (TextView)findViewById(R.id.tv_details_activity_price);
        Feature = (TextView)findViewById(R.id.tv_details_activity_feature);

        int position = getIntent().getExtras().getInt("position_id");
        List<Product> newPrductList = (List<Product>) getIntent().getSerializableExtra("productArrayList");

        Name.setText(newPrductList.get(position).getmName());
        Price.setText(newPrductList.get(position).getmPrice());
        Feature.setText(newPrductList.get(position).getmFeature());
        ProductImage.setImageResource(newPrductList.get(position).getmImage());


        btnCall = (Button)findViewById(R.id.btn_activity_details_call);
        btnSms = (Button)findViewById(R.id.btn_activity_details_sms);
        btnMail = (Button)findViewById(R.id.btn_activity_details_mail);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+"01757675333"));
                startActivity(intent);
            }
        });

        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("smsto:01757675333");
                Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(intent);
            }
        });

        btnMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","wadud8040@gmail.com", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, " ");
                intent.putExtra(Intent.EXTRA_TEXT, " ");
                startActivity(intent);
            }
        });


    }
}
