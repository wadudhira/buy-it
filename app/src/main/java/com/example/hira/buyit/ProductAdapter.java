package com.example.hira.buyit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends ArrayAdapter{

    List<Product> productsList = new ArrayList<Product>();


    public ProductAdapter(@NonNull Context context, List<Product> productsList) {
        super(context, R.layout.item,productsList);

        this.productsList = productsList;

    }





    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item,parent,false);

        ImageView imgProfile = view.findViewById(R.id.iv_item_image);
        TextView tvBrand = view.findViewById(R.id.tv_item_brand);
        TextView tvName = view.findViewById(R.id.tv_item_name);
        TextView tvPrice = view.findViewById(R.id.tv_item_price);


        imgProfile.setImageResource(productsList.get(position).getmImage());
        tvBrand.setText(productsList.get(position).getmBrand());
        tvName.setText(productsList.get(position).getmName());
        tvPrice.setText(productsList.get(position).getmPrice());

        return view;
    }

}
