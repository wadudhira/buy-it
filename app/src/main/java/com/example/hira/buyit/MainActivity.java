package com.example.hira.buyit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {




        ListView lvProductListview;
        ProductAdapter productAdapter;

        List<Product> productsList = new ArrayList<Product>();


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);


            lvProductListview =(ListView)findViewById(R.id.lv_activity_main_productslist);

            productsList.add(new Product(R.drawable.mibrand3,"Xiaomi","Xiaomi Mi Band 3 Smart Wristband Bracelet - Black","2350৳","" +
                    "Display Size: 0.78 inch\n" +
                    "Bluetooth Version: Bluetooth 4.2 \n" +
                    "Waterproof: Yes \n" +
                    "IP rating: 5ATM \n" +
                    "Screen type: OLED \n" +
                    "Operating mode: Touch Screen \n" +
                    "Compatible OS: Android, IOS \n" +
                    "Compatability: Android 4.4 / iOS 9.0 and above systems \n" +
                    "The shape of the dial: Elliptical \n" +
                    "Case material: ABS, Plastic \n" +
                    "Band material: TPU + TPE\n" +
                    "Type of battery: lithium polymer battery \n" +
                    "Battery Capacity: 110mAh \n" +
                    "Standby time: 20 days \n" +
                    "Charging Time: About 2hours"));
            productsList.add(new Product(R.drawable.nokia105,"Nokia","Nokia 105 DS 2017","1250৳","" +
                    "Display: 1.8 inches QQVGA (~10.9% screen-to-body ratio)\n" +
                    "Resolution: 120 x 160 pixels\n" +
                    "RAM: 4MB; ROM: 4MB\n" +
                    "SIM: Dual SIM (Mini-SIM, dual stand-by)\n" +
                    "Nokia Series 30+ software platform\n" +
                    "Pre-loaded Snake Xenzia\n" +
                    "Resilient battery offering up to 15 hours talk time\n" +
                    "Built-in FM radio\n" +
                    "Battery: Removable Li-Ion 800 mAh battery"));
            productsList.add(new Product(R.drawable.mi2,"Xiaomi","Xiaomi Mi Band 2","1250৳","Display: 0.42\" OLED\n" +
                    "Bluetooth: 4.0 BLE\n" +
                    "Battery capacity: 70mAh\n" +
                    "Standby time: 20 days\n" +
                    "Battery type: Lithium polymer\n" +
                    "Input current: 45 mA (typ) 65mA (max)\n" +
                    "Sensor Material: Plastic, aluminum alloy\n" +
                    "Water-Resistance: IP67"));
            productsList.add(new Product(R.drawable.nokia130,"Nokia","Nokia 130 Dual 2017","1650৳","Display: 1.8 inches (~18.6% screen-to-body ratio)\n" +
                    "Resolution: 120 x 160 pixels\n" +
                    "RAM: 4MB; ROM: 8MB\n" +
                    "Camera: VGA\n" +
                    "SIM: Dual SIM (Mini-SIM)\n" +
                    "Nokia Series 30+ software platform\n" +
                    "Pre-loaded Snake Xenzia\n" +
                    "Up to 44 hours of FM radio music playback\n" +
                    "Up to a month on standby from a single charge\n" +
                    "Built-in FM radio and MP3 player\n" +
                    "Battery: Removable Li-Ion 1020 mAh battery (BL-5C)"));


            productAdapter = new ProductAdapter(getApplicationContext(),productsList);
            lvProductListview.setAdapter(productAdapter);

            lvProductListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getApplicationContext(),DetailsActivity.class);
                    intent.putExtra("position_id",i);
                    intent.putExtra("productArrayList",(Serializable) productsList);
                    startActivity(intent);
                }
            });

        }
    }