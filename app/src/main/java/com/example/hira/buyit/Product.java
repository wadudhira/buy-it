package com.example.hira.buyit;

import java.io.Serializable;

public class Product implements Serializable{


    int mImage;
    String mBrand,mPrice,mName,mFeature;

    public Product() {

    }

    public Product (int mImage, String mBrand,String mName, String mPrice,String mFeature) {
        this.mImage = mImage;
        this.mBrand = mBrand;
        this.mPrice = mPrice;
        this.mName = mName;
        this.mFeature= mFeature;
    }

    public int getmImage() {
        return mImage;
    }

    public void setmImage(int mImage) {
        this.mImage = mImage;
    }

    public String getmBrand() {
        return mBrand;
    }

    public void setmBrand(String mBrand) {
        this.mBrand = mBrand;
    }

    public String getmPrice() {
        return mPrice;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public void setmPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public String getmFeature() {
        return mFeature;
    }

    public void setmFeature(String mFeature) {
        this.mFeature = mFeature;
    }
}
